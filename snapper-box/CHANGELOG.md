
<a name="snapper-box-v0.0.4"></a>
## [snapper-box-v0.0.4](https://gitlab.com/rust-community-matrix/snapper/compare/snapper-box-v0.0.3...snapper-box-v0.0.4)

> 2021-11-08

### Features

* **box:** Use RedactedBytes type

### BREAKING CHANGE


Remove the ConstArray type and replace it with redacted::RedactedBytes.

<a name="snapper-box-v0.0.3"></a>
## [snapper-box-v0.0.3](https://gitlab.com/rust-community-matrix/snapper/compare/snapper-box-v0.0.2...snapper-box-v0.0.3)

> 2021-11-05

### Code Refactoring

* **box:** Change module structure

### Features

* **box:** Add experimental-async feature

### Performance Improvements

* **box:** Improve performance of key derivation

### BREAKING CHANGE


This makes some modules non public, and changes where types are exported

<a name="snapper-box-v0.0.2"></a>
## [snapper-box-v0.0.2](https://gitlab.com/rust-community-matrix/snapper/compare/snapper-box-0.0.1...snapper-box-v0.0.2)

> 2021-11-03

### Features

* **box:** Utilize serde_bytes
* **box:** Add functionality to dump root namespace.
* **box:** Better json handling in box-explorer
* **box:** Add methods for accessing the root namespace
* **box:** Add box-explorer utility binary


<a name="snapper-box-0.0.1"></a>
## snapper-box-0.0.1

> 2021-11-03

### Code Refactoring

* **box:** Move modules into crypto module

### Features

* **box:** Implement CryptoBox
* **box:** Implement LsmFile struct
* **box:** Conversion between CipherText and Segment
* **box:** Make CipherText use CoW
* **box:** Implement segment type
* **box:** Create file module
* **box:** Add derive_with_context method
* **box:** Add EncryptedDerivedKey struct
* **box:** Add cipher and plain text abstractions
* **box:** Add DerivedKey type
* **box:** Implement RootKey and associated types
* **box:** Add dependencies and sketch key module
* **box:** Create box module
