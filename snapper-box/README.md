![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/rust-community-matrix/snapper?branch=trunk&style=flat-square)
[![Crates.io](https://img.shields.io/crates/v/snapper-box?style=flat-square)](https://crates.io/crates/snapper-box)
[![Matrix](https://shields.io/badge/Matrix-%23snapper%3Acommunity.rs-green?logo=matrix&style=flat-square)](https://matrix.to/#/#snapper:community.rs)
![Crates.io](https://img.shields.io/crates/l/snapper-box?style=flat-square)

# Snapper Box

<div align="center">
  <img src="https://gitlab.com/rust-community-matrix/snapper/-/raw/trunk/static/snapper.png" alt="Actually  a box turtle" width="350" height="350" />
</div>

## About

Encrypted document storage backed for the
[`snapper`](https://gitlab.com/rust-community-matrix/snapper) ecosystem.

This crate implements a multi-file, namespaced, log structured merge tree, with encrypted entries,
where each namespace has its own file.

This crate encrypts LSM entries with `XChaCha20`, and then uses Blake3 as an HMAC (an
Encrypt-then-MAC construction), with no exposed configuration in the cipher or hash, and uses a
different derived (via Blake3) key for each namespace.

The root key is generated randomly, and then encrypted with an argon2 derivation of the provided
password. This crate does not provide support for plaintext operation, passwordless operation, if
desired, should be achieved by using a null or known-constant password.

This crate provides optional support for [zstd](http://facebook.github.io/zstd/) compressed entries.

Serialized entries are stored as [CBOR](https://cbor.io/).

The root namespace is available for applications to use to store metadata or configuration, however,
be advised that [`CryptoBox`] reserves the empty-string key (`""`) and the `"namespaces"` key for
its own use, and modification of the value at the empty-string key or at the `"namespaces"` key may
result in the inability to open a [`CryptoBox`] back up.


## FAQ

### Why doesn't this use poly1305?

The author of this crate has a deep distrust of polynomial MACs, in particular, but by no means
limited to, the [non-committing nature](https://www.youtube.com/watch?v=h-T1bQTt4_Y&t=735s) of
AEAD's based on a polynomial MAC.

The implemented encryption scheme (XChaCha20 + Blake3 Encrypt-then-MAC) is commiting, unlike the
non-commiting XChaCha20 + Poly1305.

### What does <span style="color:red">**HAZMAT**</span> mean?

This tag indicates that the module that bears it contains hazardous materials, namely, the low level
cryptography underpinnings of this module. These are dangerously easy to misuse, and you probably
don't want to interact with them directly.

## Usage

TODO

### Diagnostic binary

This crate provides a diagnostics binary, `box-explorer`, run `box-explorer --help` for usage.

## Contributing

Take a look at
[`CONTRIBUTING.md`](https://gitlab.com/rust-community-matrix/snapper/-/blob/trunk/CONTRIBUTING.md)


## Documentation

Please take a look at the
[`CHANEGLOG.md`](https://gitlab.com/rust-community-matrix/snapper/-/blob/trunk/snapper-box/CHANGELOG.md)
and the rustdoc.
