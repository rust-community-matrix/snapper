//! Error types for the crate
//!
//! This module provides both [`BackendError`] for interacting with the low level APIs, and
//! [`CryptoBoxError`] for high level errors.

use snafu::Snafu;

/// Error type describing faults that can happen during encryption or decryption.
///
/// This is a fairly low level type for interacting with the api directly, you probably want
/// [`CryptoBoxError`]
#[derive(Debug, Snafu)]
#[non_exhaustive]
#[snafu(visibility(pub(crate)))]
pub enum BackendError {
    /// HMAC tag verification failed
    BadHMAC,
    /// An error occurred while deriving a key from a password
    Argon2Failure {
        /// The underlying error
        source: argon2::Error,
    },
    /// Failed to deserialize a key
    KeyDeserialization,
    /// Failed to serialize an item
    ItemSerialization,
    /// Failed to deserialize an item
    ItemDeserialization,
    /// A error occurred while attempting to compress a blob
    Compression {
        /// Underlying zstd error
        source: std::io::Error,
    },
    /// A error occurred while attempting to decompress a blob
    Decompression {
        /// Underlying zstd error
        source: std::io::Error,
    },
    /// An underlying IO error occurred while attempting to read a segment
    SegmentIO {
        /// Underlying error
        source: std::io::Error,
    },
    /// A length mismatch occurred while parsing a segment
    SegmentLength,
    /// No data was provided for a segment, not even a length
    NoData,
    /// Suspected no data provided, please see IO error for details
    NoDataIO {
        /// Underlying error
        source: std::io::Error,
    },
    /// Invalid compression flag in serialized data
    InvalidCompression,
    /// Failed reading/writing an entry
    EntryIO {
        /// Underlying error
        source: std::io::Error,
    },
    /// Indicates an invalid state where an [`LsmFile`](crate::file::LsmFile)'s internal state points to a
    /// value that doesn't exist
    InvalidLsmState,
}

/// Errors that can be returned when interacting with a [`CryptoBox`](crate::CryptoBox)
#[derive(Debug, Snafu)]
#[non_exhaustive]
#[snafu(visibility(pub(crate)))]
pub enum CryptoBoxError {
    /// Directory already exists
    #[snafu(display("Directory already exists: {}", directory))]
    DirectoryAlreadyExists {
        /// The directory that already exists
        directory: String,
    },
    /// Failed creating the `CryptoBox` directory
    FailedCreatingDirectory {
        /// The directory we couldn't create
        directory: String,
        /// The underlying IO error
        source: std::io::Error,
    },
    /// Failed to encrypt root key
    RootKeyEncryption {
        /// The underlying error
        source: BackendError,
    },
    /// Failed to decrypt root key. Most likely this is an incorrect password.
    RootKeyDecryption {
        /// The underlying error
        source: BackendError,
    },
    /// Failed to write root key file
    #[snafu(display("Failed to write to path: {}", path))]
    RootKeyIO {
        /// The path we failed to write to
        path: String,
        /// The underlying error
        source: std::io::Error,
    },
    /// [`RootKey`](crate::crypto::RootKey) serialization error
    RootKeySerial {
        /// The underlying error
        source: serde_cbor::Error,
    },
    /// Failed to initialize the root namespace
    #[snafu(display("Failed to initialize root namespace: {}", path))]
    RootNamespaceInit {
        /// The path of the root namespace
        path: String,
        /// The underlying error
        source: BackendError,
    },
    /// Failed to open root namespace
    #[snafu(display("Failed to open root namespace: {}", path))]
    RootNamespaceOpen {
        /// The path of the root namespace
        path: String,
        /// The underlying error
        source: BackendError,
    },
    /// Path does not exist or is not a directory
    #[snafu(display("Path does not exist or is not a directory: {}", path))]
    DirectoryDoesNotExist {
        /// The path that doesn't exist
        path: String,
    },
    /// Missing namespace directory
    MissingNamespaceDirectory,
    /// `CryptoBox` is missing the configuration entry
    MissingConfiguration,
    /// Failed to open a namespace
    #[snafu(display("Failed to open a namespace {}", name))]
    NamespaceOpen {
        /// The name of the namespace that failed to open
        name: String,
        /// The underlying error
        source: BackendError,
    },
    /// No such namespace
    #[snafu(display("No such namespace: {}", name))]
    NoSuchNamespace {
        /// Namespace that doesn't exist
        name: String,
    },
    /// Error retrieving item
    Fetch {
        /// Underlying failure
        source: BackendError,
    },
    /// Error storing item
    Store {
        /// Underlying failure
        source: BackendError,
    },
    /// An error occurred while flushing the `CryptoBox`
    #[snafu(display("One or more errors occurred flushing: {:?}", sources))]
    Flush {
        /// Underlying failures
        sources: Vec<(Option<String>, BackendError)>,
    },
    /// An error occurred in the async wrapper
    #[cfg(feature = "experimental-async")]
    AsyncError,
}
