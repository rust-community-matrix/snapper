use std::ops::Deref;

use serde::{de::Error, Deserialize, Serialize};
use snafu::ensure;
use zeroize::Zeroize;

use super::{GrammarMismatch, MissingComponent, NoAt, NoColon, TooLong};

/// Validates that a username matches the spec grammar
fn validate_username(id: &str) -> Result<(), GrammarMismatch> {
    /// internal helper, returns true if the character is invalid
    fn bad_char_user(ch: char) -> Option<char> {
        if ch.is_ascii_lowercase() || ch.is_digit(10) || ['.', '-', '=', '_', '/'].contains(&ch) {
            None
        } else {
            Some(ch)
        }
    }
    // Make sure length is within allowable limits
    let len = id.bytes().count();
    ensure!(
        len <= 255_usize,
        TooLong {
            max: 255_usize,
            actual: len
        }
    );
    // Check to make sure that the id starts with an @
    ensure!(
        !id.is_empty() && id.starts_with('@'),
        NoAt { id: id.to_string() }
    );
    let new_id = id.strip_prefix('@').unwrap_or("");
    // Check to make sure that the id has at least one colon
    ensure!(new_id.contains(':'), NoColon { id: id.to_string() });
    let components: Vec<&str> = new_id.split(':').collect();
    // Make sure we have the correct number of segments
    match components.len() {
        0 | 1 => Err(GrammarMismatch::MissingComponent { id: id.to_string() }),
        _ => {
            let user_part = components[0];
            let domain_part = components[1..].join(":");
            // Check the user part
            if let Some(x) = user_part.chars().find_map(bad_char_user) {
                Err(GrammarMismatch::InvaildChar {
                    id: id.to_string(),
                    ch: x,
                })
            } else {
                // Make sure we have both a name and a domain
                ensure!(
                    !user_part.is_empty() && !domain_part.is_empty(),
                    MissingComponent { id: id.to_string() }
                );
                // TODO(#1): Validate the domain part
                Ok(())
            }
        }
    }
}

/// User identifier
///
/// The string contained within must conform to the grammar given in [The Matrix
/// Spec](https://matrix.org/docs/spec/appendices#user-identifiers), and the library may not behave
/// properly if that is not the case.
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize)]
pub struct UserId(String);

impl TryFrom<String> for UserId {
    type Error = GrammarMismatch;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        validate_username(&value)?;
        Ok(Self(value))
    }
}

impl Deref for UserId {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'de> Deserialize<'de> for UserId {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let item: String = String::deserialize(deserializer)?;
        match UserId::try_from(item) {
            Ok(x) => Ok(x),
            Err(e) => Err(D::Error::custom(e)),
        }
    }
}

/// Types of identifier
///
/// Currently, this supports:
///   * `m.id.user` -- [`UserId`]
///
/// TODO(#2): Support other types of identifier
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[serde(tag = "type")]
#[non_exhaustive]
pub enum Identifer {
    /// A `m.id.user` user id
    #[serde(rename(serialize = "m.id.user", deserialize = "m.id.user"))]
    UserId {
        /// The actual underlying identifier
        user: UserId,
    },
}

impl From<UserId> for Identifer {
    fn from(user: UserId) -> Self {
        Self::UserId { user }
    }
}

/// Device identifer
///
/// This is a really thin wrapper around a `String`
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct DeviceIdentifier(pub String);

impl From<String> for DeviceIdentifier {
    fn from(x: String) -> Self {
        Self(x)
    }
}

impl Deref for DeviceIdentifier {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Password type
///
/// Special wrapper around a string that will zeroize on drop, representing a user-entered password
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Zeroize)]
#[zeroize(drop)]
pub struct Password(String);

impl From<String> for Password {
    fn from(x: String) -> Self {
        Self(x)
    }
}

impl<'de> Deserialize<'de> for Password {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let item = String::deserialize(deserializer)?;
        Ok(Self(item))
    }
}

impl Serialize for Password {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.0.serialize(serializer)
    }
}

impl Deref for Password {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Authentication token type
///
/// Special wrapper around a string that will zeroize on drop, representing an authentication token
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Zeroize)]
#[zeroize(drop)]
pub struct AuthenticationToken(String);

impl From<String> for AuthenticationToken {
    fn from(x: String) -> Self {
        Self(x)
    }
}

impl<'de> Deserialize<'de> for AuthenticationToken {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let item = String::deserialize(deserializer)?;
        Ok(Self(item))
    }
}

impl Serialize for AuthenticationToken {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.0.serialize(serializer)
    }
}

impl Deref for AuthenticationToken {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Unit tests
#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::{from_str, to_string};

    /// Tests for `UserId`
    mod user_id {
        use std::string::ToString;

        use super::*;
        /// Test round tripping `UserId`s
        #[test]
        fn user_id_round_trip() {
            let raw_id = "@nathan:mccarty.io".to_string();
            let id = UserId::try_from(raw_id.clone()).expect("Failed to parse valid user id");

            let serilized_id = to_string(&id).expect("Failed to serialize");
            assert_eq!(serilized_id, format!("\"{}\"", raw_id));
            let deser_id: UserId = from_str(&serilized_id).expect("Failed to deser");
            assert_eq!(deser_id, id);
        }

        /// Make sure we reject `UserId`s that don't start with an `@`
        #[test]
        fn reject_no_at() {
            // Otherwise correct, but missing the @
            let id = "nathan:mccarty.io".to_string();
            assert_eq!(
                UserId::try_from(id.clone()),
                Err(GrammarMismatch::NoAt { id })
            );
            // Empty string
            let id = String::new();
            assert_eq!(
                UserId::try_from(id.clone()),
                Err(GrammarMismatch::NoAt { id })
            );
        }

        /// Make sure we reject `UserId`s that don't have a `:`
        #[test]
        fn reject_no_colon() {
            let id = "@nathanmccarty.io".to_string();
            assert_eq!(
                UserId::try_from(id.clone()),
                Err(GrammarMismatch::NoColon { id })
            );
        }

        /// Make sure we reject `UserId`s that don't have a `:`
        #[test]
        fn reject_invalid_chars() {
            // Upper case letter
            let id = "@nathAn:mccarty.io".to_string();
            assert_eq!(
                UserId::try_from(id.clone()),
                Err(GrammarMismatch::InvaildChar { id, ch: 'A' })
            );
            // unicode
            let id = "@nathan\u{44a}:mccarty.io".to_string();
            assert_eq!(
                UserId::try_from(id.clone()),
                Err(GrammarMismatch::InvaildChar { id, ch: '\u{44a}' })
            );
        }

        /// Batch acceptance test
        #[test]
        fn batch_acceptance() {
            let names = ["@nathan:mccarty.io", "@tim:1.2.3.4", "@jim:thing.com:1234"]
                .into_iter()
                .map(ToString::to_string);
            for name in names {
                let id = UserId::try_from(name.clone());
                if let Err(e) = id {
                    panic!("Failed to parse id: {} with error: {}", name, e)
                }
            }
        }

        /// Batch rejection
        #[test]
        fn batch_rejection() {
            let names = [
                "@Nathan:mccarty.io",
                "@:1.2.3.4",
                "@:thing.com",
                "@:thing:1234",
            ]
            .into_iter()
            .map(ToString::to_string);
            for name in names {
                let id = UserId::try_from(name.clone());
                if id.is_ok() {
                    panic!("Invalid id validated: {}", name);
                }
            }
        }
    }

    /// Tests for `Identifier`
    mod ident {
        use super::*;
        /// Make sure the type serializes correctly
        #[test]
        fn serialization_sanity() {
            let id = UserId("@nathan:mccarty.io".to_string());
            let id = Identifer::from(id);
            let string = serde_json::to_string(&id).unwrap();
            assert_eq!(
                &string,
                r#"{"type":"m.id.user","user":"@nathan:mccarty.io"}"#
            );
        }
    }
}
