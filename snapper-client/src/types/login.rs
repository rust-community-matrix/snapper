use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use snafu::{ResultExt, Snafu};
use snapper_box::async_wrapper::AsyncCryptoBox;
use surf::Url;
use tracing::{debug, instrument};

use crate::{
    state::LoginState,
    types::{
        identifiers::{AuthenticationToken, DeviceIdentifier, Identifer, Password, UserId},
        matrix_error::MatrixError,
        traits::request::{
            post, Header, Request, RequestError, Response, ResponseError, StoreError,
        },
    },
    Client, ClientState,
};

/// Error for login requests
///
/// TODO(#4): Support all relevant error types
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Snafu)]
pub enum LoginError {
    /// An unspecified matrix error occurred
    #[snafu(display("Unspecified matrix error occured: {:?}", error))]
    Unspecified {
        /// The underlying matrix error
        error: MatrixError,
    },
}

impl ResponseError for LoginError {}

/// [`/_matrix/client/r0/login`](https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-login)
/// POST request body
///
/// Currently supports:
///   * `m.login.password`
///   * `m.login.token`
///
/// TODO(#2): support all login types
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[serde(tag = "type")]
#[non_exhaustive]
pub enum Login {
    /// A `m.login.password` login request
    #[serde(rename(serialize = "m.login.password", deserialize = "m.login.password"))]
    Password {
        /// The identifier used for the attempt
        identifier: Identifer,
        /// The actual password
        ///
        /// TODO(#3) zeroize passwords
        password: Password,
        /// The device id
        ///
        /// This is optional, and if one is not provided, the server will set and return one.
        device_id: Option<DeviceIdentifier>,
    },
    /// A `m.login.token` login request
    #[serde(rename(serialize = "m.login.token", deserialize = "m.login.token"))]
    Token {
        /// The identifier used for the attempt
        identifier: Identifer,
        /// The token used for the attempt
        ///
        /// TODO(#3) zeroize passwords
        token: AuthenticationToken,
        /// Transaction id for the event, this should be random
        txn_id: String,
        /// The device id
        ///
        /// This is optional, and if one is not provided, the server will set and return one.
        device_id: Option<DeviceIdentifier>,
    },
}

impl Login {
    /// Creates a new login request from a username and password
    pub fn password(
        username: UserId,
        password: Password,
        device_id: Option<DeviceIdentifier>,
    ) -> Login {
        Login::Password {
            identifier: Identifer::UserId { user: username },
            password,
            device_id,
        }
    }

    /// Creates a new login request from a session token
    ///
    /// Internally generates a random `txn_id`
    pub fn token(
        username: UserId,
        token: AuthenticationToken,
        device_id: Option<DeviceIdentifier>,
    ) -> Login {
        let uuid = uuid::Uuid::new_v4();
        Login::Token {
            identifier: Identifer::UserId { user: username },
            token,
            txn_id: uuid.to_string(),
            device_id,
        }
    }
}

/// [`Login`] Response Type
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct LoginResponse {
    /// The [`UserId`] that has been registered
    user_id: UserId,
    /// The access token associated with this session
    access_token: AuthenticationToken,
}

#[async_trait]
impl Request for Login {
    type Response = LoginResponse;
    type Error = LoginError;
    #[instrument(skip(headers))]
    async fn perform_request(
        &self,
        api_endpoint: &Url,
        headers: &Header,
        client: &surf::Client,
    ) -> Result<Self::Response, RequestError<Self::Error>> {
        let url = api_endpoint.join("_matrix/client/r0/login")?;
        debug!(?url, "Posting");
        post(client, url, self, headers, Ok).await
    }
}

#[async_trait]
impl Response for LoginResponse {
    type Error = LoginError;
    async fn state_transition<'a>(&self, client: Client) -> Result<(), RequestError<Self::Error>> {
        // Change the login state to "LoggedIn" and update the store
        let mut store = client.store.write().await;
        let store: &mut AsyncCryptoBox = &mut *store;
        let mut state = client.state.write().await;
        let state: &mut ClientState = &mut *state;
        // Update the state
        state.login_state = LoginState::LoggedIn {
            auth_token: self.access_token.clone(),
        };
        // Update the store
        store
            .insert(
                &"login-state",
                &LoginState::LoggedIn {
                    auth_token: self.access_token.clone(),
                },
                "client",
            )
            .await
            .context(StoreError)?;
        Ok(())
    }
}
